# Brainfuck

An interpreter for the [Brainfuck programming language](https://en.wikipedia.org/wiki/Brainfuck) written in rust.

## Usage

    Usage: bf [-h] [-d] [-f FILE]
    
    An interpreter for the Brainfuck programming language
    
    Options:
        -h, --help          Print help message and exit.
        -d, --debug         Print debug information to stderr as the program
                            executes
        -f, --file FILE     Specifies the program file to run

## Installation

### Manual

Download the archive for your operating system (currently only linux i586 and x86\_64 with glibc and musl have precompiled binary releases) from the releases. Extract the archive and place the `bf` binary in a directory in your PATH.

### Cargo

In a terminal/cmd, run

`cargo install --git https://gitlab.com/jcheatum/brainfuck`

