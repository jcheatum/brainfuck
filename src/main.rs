use std::env;
use std::fs;
use std::process;

use args::*;
use getopts::Occur;

mod run;
mod loop_map;
use run::run;

const PROG_NAME: &str = "bf";
const PROG_DESC: &str = "An interpreter for the Brainfuck programming language";

fn main() {
    let argv: Vec<String> = env::args().collect();

    let mut args = Args::new(PROG_NAME, PROG_DESC);
    args.flag("h", "help", "Print help message and exit.");
    args.flag("d", "debug", "Print debug information to stderr as the program executes");
    args.option("f", "file", "Specifies the program file to run", "FILE", Occur::Optional, None);
    match args.parse(argv.clone()) {
        Ok(()) => (),
        Err(e) => {
            eprintln!("{}: {}", argv[0], e);
            process::exit(1);
        }
    };

    if args.value_of("help").unwrap() {
        println!("{}", args.full_usage());
        process::exit(0);
    }

    let filename: String = match args.value_of("file") {
        Ok(s) => s,
        Err(_) => {
            eprintln!("{}: Error: missing file argument", argv[0]);
            process::exit(2);
        }
    };

    let program: Vec<u8> = match fs::read(&filename) {
        Ok(s) => s,
        Err(_) => {
            eprintln!("{}: Error: file {} not found", argv[0], filename);
            process::exit(3);
        }
    };

    match run(&program, args.value_of("debug").unwrap()) {
        Ok(()) => (),
        Err(s) => {
            eprintln!("{}: Error: {}", argv[0], s);
            process::exit(2);
        }
    };
    println!();
}    

