use std::collections::HashMap;

pub struct LoopMap {
    by_open_idx: HashMap<usize, usize>,
    by_close_idx: HashMap<usize, usize>
}

impl LoopMap {
    pub fn new() -> LoopMap {
        LoopMap {
            by_open_idx: HashMap::new(),
            by_close_idx: HashMap::new()
        }
    }

    pub fn insert(&mut self, open: &usize, close: &usize) {
        self.by_open_idx.insert(*open, *close);
        self.by_close_idx.insert(*close, *open);
    }

    pub fn get_closing(&self, open: &usize) -> Option<usize> {
        if let Some(close) = self.by_open_idx.get(open) {
            Some(*close)
        } else {
            None
        }
    }

    pub fn get_opening(&self, close: &usize) -> Option<usize> {
        if let Some(open) = self.by_close_idx.get(close) {
            Some(*open)
        } else {
            None
        }
    }
}
