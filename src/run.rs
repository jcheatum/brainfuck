use std::io::{self , Write};

use scanner_rust::ScannerAscii;

use crate::loop_map::LoopMap;

const NUM_CELLS: usize = 30000;

pub fn run(program: &Vec<u8>, debug: bool) -> Result<(), String> {
    let mut stdin = ScannerAscii::new(io::stdin());
    let mut cells: [u8; NUM_CELLS] = [0; NUM_CELLS];
    let mut ptr = 0;
    
    let mut loops = LoopMap::new();
    for (i, ch) in program.iter().enumerate() {
        if *ch == b'[' {
            loops.insert(
                &i, 
                &match find_closing_bracket(&program, i) {
                    Ok(n) => n,
                    Err(s) => {
                        return Err(s);
                    }
                }
            );
        } else if *ch == b']' {
            loops.insert(
                &match find_opening_bracket(&program, i) {
                    Ok(n) => n,
                    Err(s) => {
                        return Err(s);
                    }
                },
                &i
            );
        }
    }

    let mut i = 0;
    if debug {eprintln!("ptr:cell    instr#:instr");}
    while i < program.len() {
        if debug {
            eprintln!("\n{}:{}         {}:{}", 
                ptr, 
                cells[ptr],
                i,
                program[i] as char,
            );
        }
        match program[i] {
            b'+' => cells[ptr] += 1,
            b'-' => cells[ptr] -= 1,
            b'>' => ptr += 1,
            b'<' => ptr -= 1,

            b'[' => {
                if cells[ptr] == 0 {
                    i = loops.get_closing(&i).unwrap();
                }
            },

            b']' => {
                if cells[ptr] != 0 {
                    i = loops.get_opening(&i).unwrap();
                } 
            },

            b'.' => {
                print!("{}", cells[ptr] as char);
                io::stdout().flush().ok().expect("Error: could not flush stdout");
            },

            b',' => cells[ptr] = stdin.next_char().unwrap().expect("Error reading from stdin") as u8,

            _ => () 
        }
        i += 1;
    }
    Ok(())
}

fn find_closing_bracket(prog: &Vec<u8>, index: usize) -> Result<usize, String> {
    let to_search = &prog[index..];
    let mut loop_stack = Vec::new();
    for (i, ch) in to_search.iter().enumerate() {
        if *ch == b'[' {
            loop_stack.push(i);
        } else if *ch == b']' {
            if loop_stack[0] == loop_stack.pop().unwrap() {
                return Ok(index+i);
            }
        }
    }
    
    let mut error = String::from("missing closing bracket for loop at character ");
    error.push_str(index.to_string().as_str());
    Err(error)
}

fn find_opening_bracket(prog: &Vec<u8>, index: usize) -> Result<usize, String> {
    let to_search = &prog[0..=index];
    let mut loop_stack = Vec::new();
    for (i, ch) in to_search.iter().enumerate().rev() {
        if *ch == b']' {
            loop_stack.push(i);
        } else if *ch == b'[' {
            if loop_stack[0] == loop_stack.pop().unwrap() {
                return Ok(i);
            }
        }
    }

    let mut error = String::from("missing opening bracket for loop at character ");
    error.push_str(index.to_string().as_str());
    Err(error)
}
